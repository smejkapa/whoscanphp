<style>
td{
  cursor: pointer;
}
</style>

<?php
if (!empty($_GET["guild"])):

query_show("");
else:

$g_limit = "LIMIT 40";

echo '<h1>Guild list</h1>';


echo '<div class="col-lg-3">';
echo '<h2>Members</h2>';
query_show("SELECT guild, count(*) as `players` FROM (SELECT guild, name FROM ".TABLE_NAME." GROUP BY guild, name) as guild_and_player GROUP BY guild ORDER BY players DESC, guild ASC $g_limit");
echo '</div>';


echo '<div class="col-lg-3">';
echo '<h2>Members lvl 70+</h2>';
query_show("SELECT guild, count(*) as `players` FROM (SELECT guild, name, max(lvl) as lvl FROM ".TABLE_NAME." GROUP BY guild, name HAVING lvl >= 70) as guild_and_player GROUP BY guild ORDER BY players DESC, guild ASC $g_limit");
echo '</div>';


echo '<div class="col-lg-3">';
echo '<h2>Members lvl 80</h2>';
query_show("SELECT guild, count(*) as `players` FROM (SELECT guild, name, max(lvl) as lvl FROM ".TABLE_NAME." GROUP BY guild, name HAVING lvl = 80) as guild_and_player GROUP BY guild ORDER BY players DESC, guild ASC $g_limit");
echo '</div>';

endif;


?>

<script>

$("td").click(function(){
  $.each($("tr"), function(){
    $(this).removeClass("bg-info");
  });
  findAndHighlight(this.innerHTML);
})

function findAndHighlight(text){
  $.each($("td:contains(" + text + ")"), function(){
    $($(this).parent()).addClass("bg-info");
  });
}

</script>

