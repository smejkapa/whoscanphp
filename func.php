<?php 




function display_table($query_result, array $function_list = array())
{
	$column_names = $query_result->fetch_fields();
	echo "<table class=\"table table-hover table-bordered\" style=\"width: auto;\">";


	echo "<tr>";
	echo "<th></th>";
	foreach($column_names as $c_name)
	{
		echo "<th>".$c_name->name."</th>";
	}
	echo "</tr>";

	$row_number = 1;

	while ($row = $query_result->fetch_assoc())
	{
		echo "<tr>";
		echo "<td>".$row_number++."</td>";
		foreach($column_names as $c_name)
		{
			$show = $row[$c_name->name];
			
			foreach ($function_list as $func)
			{
				$show = $func($c_name, $show);
			}
			
			
			echo "<td>".$show."</td>";
		}
		echo "</tr>";
	}

	echo "</table>";

};

function player_link($player)
{
	return "<a href=\".?page=player&player=$player\">".$player."</a>";
}

function df_player($c_name, $player)
{
	if ($c_name->name == "name") 
	{
		return player_link($player);
	}
	else 
	{
		return $player;
	}
}

function query_show($query)
{
	global $db;
	$result = $db->query($query);
	display_table($result, array("df_player"));
}



?>
