<?php
require("conn.php");

if (!empty($_POST['file'])):


$text = trim($_POST['file']);
$textAr = explode("\n", $text);
$textAr = array_filter($textAr, 'trim');

$q_begin = "INSERT IGNORE INTO ".TABLE_NAME." (scan_time, name, guild, lvl, race, class, zone) VALUES ";
$query = $q_begin;
$count = 0;
$limit = 400;

foreach ($textAr as $line) {
	$parts = explode(";", $line);
	if ($count > 0) $query.= ", ";
	$query.= "(";
	
	foreach ($parts as $part)
	{
		if ($part >= 1 and $part <= 80) //is number? wow levels... only column with a number
		{
			$query.="$part,";
		}
		else
		{
			$query.="\"$part\",";
		}
	}
	
	$query[strlen($query)-1]=")";
	
	$count++;
	
	if ($count >= $limit) 
	{
		$db->query($query);
		$query = $q_begin;
		$count = 0;
	}

} 

if ($count > 0 ) 
{
	$db->query($query);
}

if ($db->error)
{
	echo "Something happened";
}
else
{
	echo "Done";
}

else:
?>

<form method="post">
	<textarea name="file"></textarea>
	<input type="submit">
</form>

<?php
endif;
?>
